from allure_commons.types import Severity
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
import allure


@allure.title('Отчет теста test_search_001')  # задаем имя отчета для теста
@allure.severity(Severity.BLOCKER)  # задаем серьезность теста

def test_google_search(): # тестовый метод

    driver = WebDriver(executable_path='c:\Project_Radchenko\chromedriver.exe') # создаем экземпляр класса driver

    with allure.step('Перейти на страницу google'): # группируем тест по шагам
        driver.get('https://www.google.com.ua/?hl=ru')

    with allure.step('Найти rybolov-expert.com.ua'):
        search_input = driver.find_element_by_xpath('//*[@id="tsf"]/div[2]/div[1]/div[1]/div/div[2]/input')
        search_input.send_keys('rybolov-expert.com.ua')
        search_button = driver.find_element_by_xpath('//*[@name="btnK"]')
        search_button.click()
        search_element = driver.find_element_by_xpath('//*[@id="rso"]/div[1]/div/div/div/div/div[1]/a') # найти ссылку rybolov-expert.com.ua
        search_element.click()

    def check_menu_count(driver): # метод проверки
        search_main_menues = driver.find_elements_by_xpath('//*[@class="tv_link_name"]')  # найдем элеменнты главного меню
        return len(search_main_menues) == 10 # если элементов будет 10 - вернятся true, если нет - ошибка

    with allure.step('Ожидаем что количество меню в тесте будет равно 10'):
        WebDriverWait(driver, 10, 0.5).until(check_menu_count, 'Количество меню не равно 10')

    with allure.step('Перейти в меню Каталог'):
        search_main_menues_new = driver.find_elements_by_xpath('//*[@class="tv_link_name"]')
        menu_Katalog = driver.find_element_by_xpath('//td[1]//a[1]//span[2]').click()

    driver.switch_to.window(driver.window_handles[0]) # метод/команда переключения во первое окно

    with allure.step('Проверить корректность тайтла страницы'):
        assert driver.title == 'Каталог рыболовных снастей, оснаток, приманок, прикормок ― Рыболов-Эксперт'  # Метод .title - проверяет тайтл странички
